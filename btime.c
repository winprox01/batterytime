#include <pebble.h>
static Window *w;
static TextLayer *l;
static void t(struct tm *t, TimeUnits changed) {
  time_t tm = time(NULL);
  t = localtime(&tm);
  static char s[7];
  strftime(s, sizeof(s), clock_is_24h_style() ? "%H:%M" : "%I:%M", t);
  text_layer_set_text(l, s);
}
static void window_load(){
  Layer *r = window_get_root_layer(w);
  tick_timer_service_subscribe(MINUTE_UNIT, t);
  l = text_layer_create(GRect(0, 0, 144, 168));
  text_layer_set_background_color(l, GColorBlack);
  text_layer_set_text_color(l, GColorWhite);
  text_layer_set_font(l, fonts_get_system_font(FONT_KEY_GOTHIC_24));
  text_layer_set_text_alignment(l, GTextAlignmentCenter);
  layer_add_child(r, text_layer_get_layer(l));
}
int main(void){
  w = window_create();
  window_set_window_handlers(w, (WindowHandlers){.load = window_load});
  window_stack_push(w, false);
  app_event_loop();
  window_destroy(w);
  text_layer_destroy(l);
  tick_timer_service_unsubscribe();
}